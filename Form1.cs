using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp2
{
    public partial class Form1 : Form
    {
        public delegate void MyDelegate(ArrayList subscribers);
        public MyDelegate arrSubscribers = null;

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var subscribers = new ArrayList { "user1", "user2" };
            arrSubscribers.Invoke(subscribers);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var form2 = new Form2(this);
            form2.Show();
        }
    }
}
